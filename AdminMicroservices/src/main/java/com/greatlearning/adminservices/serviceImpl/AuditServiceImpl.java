package com.greatlearning.adminservices.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.adminservices.entity.Audit;
import com.greatlearning.adminservices.repository.AuditRepository;
import com.greatlearning.adminservices.service.AuditService;

@Service
public class AuditServiceImpl implements AuditService {

	// Audit Service Impl to get the sales details
	@Autowired
	private AuditRepository auditRepository;

	// gets the bill for the day
	@Override
	public List<Audit> getBillForToday() {
		return auditRepository.getAuditForToday();
	}

	// gets the sales for the month
	@Override
	public List<Audit> getSalesForTheMonth() {
		return auditRepository.getAuditForCurrentMonth();
	}

}
