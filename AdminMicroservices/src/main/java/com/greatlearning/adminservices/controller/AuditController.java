package com.greatlearning.adminservices.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.adminservices.entity.Audit;
import com.greatlearning.adminservices.service.AuditService;

@RestController
@RequestMapping("/Restaurants/Audit")
public class AuditController {

	// Audit controller to retrieve the sales details from Audit table
	@Autowired
	private AuditService auditService;

	// gets the sales for the day
	@GetMapping("/getBillsForToday")
	public List<Audit> getBillsForToday() {

		return auditService.getBillForToday();
	}

	// gets the sales for the month
	@GetMapping("/getSalesForTheMonth")
	public List<Audit> getSalesForTheMonth() {

		return auditService.getSalesForTheMonth();
	}
}
