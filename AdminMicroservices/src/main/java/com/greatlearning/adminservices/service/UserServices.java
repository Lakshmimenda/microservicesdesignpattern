package com.greatlearning.adminservices.service;

import java.util.List;
import com.greatlearning.adminservices.entity.User;

public interface UserServices {

	// User Services
	public List<User> getUsers();

	public String getUsersbyId(Long id);

	public String updateUser(Long id, User user);

	public String registerUser(User user);

	public String deleteUser(Long id);

}
