package com.greatlearning.adminservices.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.greatlearning.adminservices.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	// User Repository

}
