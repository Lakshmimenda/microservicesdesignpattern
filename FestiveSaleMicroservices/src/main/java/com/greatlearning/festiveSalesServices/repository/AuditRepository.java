package com.greatlearning.festiveSalesServices.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.festiveSalesServices.entity.Audit;

@Repository
public interface AuditRepository extends JpaRepository<Audit, Long> {

	// Audit Repository

	// Query to add in the Coupon code under the user
	@Transactional
	@Modifying
	@Query("update Audit a set a.code='Special50' where a.userName = :name")
	public void updateCoupon(@Param("name") String userName);

}
