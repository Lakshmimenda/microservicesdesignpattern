package com.greatlearning.UserMicrosServices.service;

import java.util.Collection;
import java.util.List;

import com.greatlearning.UserMicrosServices.entity.Items;

public interface ItemsServices {

	// Items Services that allows the user to preform various users action

	// gets all the items
	public List<Items> viewItems();

	// gets the item by id
	public String checkItemExists(Long id);

	// selects the items by id
	public List<Items> selectedItems(Collection<Long> ids, String userName);

	// gets the total bill
	public String totalBill(String userName, String code);
}
