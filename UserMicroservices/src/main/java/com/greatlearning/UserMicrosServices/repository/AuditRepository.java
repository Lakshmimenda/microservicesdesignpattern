package com.greatlearning.UserMicrosServices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.UserMicrosServices.entity.Audit;

@Repository
public interface AuditRepository extends JpaRepository<Audit, Long> {

	// Audit Repository
}
