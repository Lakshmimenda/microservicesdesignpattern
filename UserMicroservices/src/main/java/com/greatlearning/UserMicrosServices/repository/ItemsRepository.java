package com.greatlearning.UserMicrosServices.repository;

import org.springframework.stereotype.Repository;

import com.greatlearning.UserMicrosServices.entity.Items;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ItemsRepository extends JpaRepository<Items, Long> {

	// Items Repository
}