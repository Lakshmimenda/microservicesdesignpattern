package com.greatlearning.UserMicrosServices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;


@SpringBootApplication
public class UserMicroservices {

	public static void main(String[] args) {
		SpringApplication.run(UserMicroservices.class, args);
	}

}
