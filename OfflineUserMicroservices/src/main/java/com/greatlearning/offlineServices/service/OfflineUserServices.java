package com.greatlearning.offlineServices.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.greatlearning.offlineServices.entity.Items;

public interface OfflineUserServices {

	// Offline user service Interface
	// gets all the items
	public List<Items> viewItems();

	// gets the item by id
	public String checkItemExists(Long id);

	// allows the user to select the items
	public Map<Items, Integer> selectedItems(Map<Long, Integer> selectedItems, String userName);

	// gets the total bill
	public BigInteger totalBill(String userName);

	// allows the user to book events
	public String bookEvent(String date) throws ParseException;

	// allows the user to enter the feedback
	public String feedback();

	// allows the user to make payments
	public String makePayment(String paymentMode, String code, String userName);
}
