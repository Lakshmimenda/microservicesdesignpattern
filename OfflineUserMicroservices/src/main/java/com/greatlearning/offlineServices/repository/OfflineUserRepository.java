package com.greatlearning.offlineServices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatlearning.offlineServices.entity.Items;

@Repository
public interface OfflineUserRepository extends JpaRepository<Items, Long> {

	// Offline User Items actions Repository
}
