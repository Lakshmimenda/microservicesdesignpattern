package com.greatlearning.offlineServices.serviceImpl;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.offlineServices.entity.Audit;
import com.greatlearning.offlineServices.entity.Items;
import com.greatlearning.offlineServices.repository.AuditRepository;
import com.greatlearning.offlineServices.repository.OfflineUserRepository;
import com.greatlearning.offlineServices.service.OfflineUserServices;

@Service
public class OfflineUserServiceImpl implements OfflineUserServices {

	// Offline User Service Impl
	@Autowired
	private EntityManager entityManager;

	@Autowired
	private OfflineUserRepository offlineUserRepository;

	@Autowired
	private AuditRepository auditRepository;

	// gets all the items details
	public List<Items> viewItems() {
		return offlineUserRepository.findAll();
	}

	// gets the item by id
	public String checkItemExists(Long id) {
		if (!offlineUserRepository.findById(id).isPresent()) {
			return "Item Does Not Exists";

		} else {
			return "Item Details : " + offlineUserRepository.getById(id);
		}
	}

	// allows the user to select the items
	public Map<Items, Integer> selectedItems(Map<Long, Integer> items, String userName) {
		List<Items> selectedItems = offlineUserRepository.findAllById(items.keySet());
		HashMap<Items, Integer> resultSet = new HashMap<Items, Integer>();
		selectedItems.forEach(item -> {
			int price = item.getPrice() * items.get(item.getId());
			auditRepository.saveAndFlush(new Audit(userName, item.getName(), price, new Date()));
			resultSet.put(item, price);
		});

		return resultSet;
	}

	// gets the total bill
	public BigInteger totalBill(String userName) {

		String queryStr = "select sum(item_price) from audit where user_name = ?1";
		try {
			Query query = entityManager.createNativeQuery(queryStr);
			query.setParameter(1, userName);

			return (BigInteger) query.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
	}

	// allows the user to book event after 2 days
	public String bookEvent(String date) throws ParseException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date availableEventDate = dateFormat
				.parse(dateFormat.format(new Date(System.currentTimeMillis() + 2L * 24 * 3600 * 1000)));
		Date enteredDate = dateFormat.parse(date);
		if (enteredDate.equals(availableEventDate)) {
			return "Booking Successful ";
		} else {
			return "Please Try Again.The booking can be done only 2 day before the actual event";
		}
	}

	// gets user feedback
	public String feedback() {
		return "Thanks for the Feedback. Have a good day!";
	}

	// allows user to make payments
	public String makePayment(String paymentMode, String code, String userName) {
		if (paymentMode.equalsIgnoreCase("cash")) {
			return "Thanks for selecting the Payment Mode as Cash" + "\nThe Total Bill is " + totalBill(userName);
		} else {
			StringBuffer sb = new StringBuffer();
			sb.append("Thanks for selecting the Payment Mode as card.");

			if (!code.isEmpty()) {
				if (isCouponApplies(userName, code)) {
					sb.append("\nThe Total Bill is " + (Integer.parseInt(totalBill(userName).toString()) * 50) / 100
							+ "\nCoupon Successfully Applied");
				} else {
					sb.append("\nInvalid Coupon Code");
					sb.append("\nThe Total Bill is " + totalBill(userName));
				}
			} else {
				sb.append("\nThe Total Bill is " + totalBill(userName));
			}
			return sb.toString();
		}

	}

	// checks if the coupon entered is applicable
	public boolean isCouponApplies(String userName, String code) {

		String queryStr = "select code from audit where user_name = ?1 and code is not null";
		try {
			Query query = entityManager.createNativeQuery(queryStr);
			query.setParameter(1, userName);

			List result = query.getResultList();
			return result.contains(code);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		}
	}

}
