package com.greatlearning.springbootrestaurantsapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.springbootrestaurantsapp.entity.*;
import com.greatlearning.springbootrestaurantsapp.feignClients.AdminClient;

@RestController
@RequestMapping("/Restaurants/Admin")
public class AdminController {

	// Admin Controller to perform CRUD on Users and View Bills for Month and Today.
	@Autowired
	private AdminClient adminClient;

	// gets all the users details
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers() {
		return adminClient.getAllUsers();
	}

	// gets user details by id
	@GetMapping("/getUserById")
	public String getUserById(Long id) {
		return adminClient.getUserById(id);
	}

	// registering the user
	@PostMapping("/registerUser")
	public String registerUser(@Validated @RequestBody User user) {
		return adminClient.registerUser(user);
	}

	// updating user based on id
	@PutMapping("/updateUser")
	public String updateUser(Long id, @RequestBody User user) {
		return adminClient.updateUser(id, user);
	}

// deleting user based on id
	@DeleteMapping("/deleteUser")
	public String deleteUser(Long id) {
		return adminClient.deleteUser(id);
	}

	// gets bill for today's transaction
	@GetMapping("/getBillsForToday")
	public List<Audit> getBillsForToday() {

		return adminClient.getBillsForToday();
	}

	// gets sales for the month
	@GetMapping("/getSalesForTheMonth")
	public List<Audit> getSalesForTheMonth() {

		return adminClient.getSalesForTheMonth();
	}

}
