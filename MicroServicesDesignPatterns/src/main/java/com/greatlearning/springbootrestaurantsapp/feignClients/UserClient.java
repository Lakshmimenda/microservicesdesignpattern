package com.greatlearning.springbootrestaurantsapp.feignClients;

import java.util.Collection;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.greatlearning.springbootrestaurantsapp.entity.*;

@FeignClient(url = "http://localhost:8082", name = "User-Client")
public interface UserClient {

	// User Fiegn Client

	// gets the item details by id
	@GetMapping("/Restaurants/Items/checkItem")
	public String checkItemExist(@RequestParam("id") Long id);

	// gets all the items details
	@GetMapping("/Restaurants/Items/viewItems")
	public List<Items> viewItems();

	// allows the user to select the items
	@PostMapping("/Restaurants/Items/selectItems")
	public List<Items> selectedItems(@RequestBody Collection<Long> ids, @RequestParam("userName") String userName);

	// gets the total bill
	@GetMapping("/Restaurants/Items/totalBill")
	public String totalBill(@RequestParam("userName") String userName, @RequestParam("code") String code);

}
